package com.example.javaindexinges.controller;

import com.example.javaindexinges.model.Book;
import com.example.javaindexinges.service.BookService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
public class BookController {

    @Autowired
    private BookService bookService;


    @PutMapping("/books")
    public Book saveBook(@RequestBody Book book) throws IOException {
        return bookService.save(book);
    }

}

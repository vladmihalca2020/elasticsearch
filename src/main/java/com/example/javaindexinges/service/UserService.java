package com.example.javaindexinges.service;

import com.example.javaindexinges.elasticDAO.ElasticsearchDAO;
import com.example.javaindexinges.model.User;
import com.example.javaindexinges.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final ElasticsearchDAO elasticsearchDAO;

    @Autowired
    public UserService(UserRepository userRepository, ElasticsearchDAO elasticsearchDAO) {
        this.userRepository = userRepository;
        this.elasticsearchDAO = elasticsearchDAO;
    }

    public User save(User user){
        userRepository.save(user);
        try {
            elasticsearchDAO.save(user);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return user;
    }
}

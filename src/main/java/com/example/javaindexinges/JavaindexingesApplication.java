package com.example.javaindexinges;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaindexingesApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaindexingesApplication.class, args);
    }

}

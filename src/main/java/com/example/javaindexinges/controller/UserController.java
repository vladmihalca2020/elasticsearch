package com.example.javaindexinges.controller;

import com.example.javaindexinges.model.User;
import com.example.javaindexinges.repository.UserRepository;
import com.example.javaindexinges.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @PutMapping("/user")
    public User saveUser(@RequestBody User user){
        return userService.save(user);
    }

    @GetMapping("/getall")
    public List<User> getAllUsers(){
        return (List<User>) userRepository.findAll();
    }
}

package com.example.javaindexinges.service;

import com.example.javaindexinges.elasticDAO.ElasticsearchDAO;
import com.example.javaindexinges.model.Author;
import com.example.javaindexinges.model.Book;
import com.example.javaindexinges.repository.BookRepository;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.Strings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.management.Query;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class BookService {
    private final Logger logger = LoggerFactory.getLogger(Book.class);
    private final BookRepository bookRepository;
    private final ElasticsearchDAO elasticsearchDAO;

    @Autowired
    public BookService(BookRepository bookRepository, ElasticsearchDAO elasticsearchDAO) {
        this.bookRepository = bookRepository;
        this.elasticsearchDAO = elasticsearchDAO;
    }

    public Book save(Book book){
        bookRepository.save(book);
        try{
            elasticsearchDAO.save(book);
        }   catch (Exception e){
            e.printStackTrace();
        }
        return book;
    }



    /*public String search(String q) throws IOException {
        QueryBuilder query;

        if(Strings.isEmpty(q)){
            query = QueryBuilders.matchAllQuery();
        }
        else {
            query = QueryBuilders
                    .multiMatchQuery(q)
                    .field("title");
        }

        SearchResponse response = elasticsearchDAO.search(query);

        if(logger.isDebugEnabled()) logger.debug("search({})={} books", q, response.getHits().getTotalHits());

        return response.toString();

    }*/


}

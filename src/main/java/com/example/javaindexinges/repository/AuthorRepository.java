package com.example.javaindexinges.repository;

import com.example.javaindexinges.model.Author;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface AuthorRepository extends ElasticsearchRepository<Author, String> {
}

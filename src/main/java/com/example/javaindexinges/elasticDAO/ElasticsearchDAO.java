package com.example.javaindexinges.elasticDAO;

import com.example.javaindexinges.model.Author;
import com.example.javaindexinges.model.Book;
import com.example.javaindexinges.model.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.MainResponse;
import org.elasticsearch.client.ml.PostDataRequest;
import org.elasticsearch.client.ml.job.process.DataCounts;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class ElasticsearchDAO {

    private final Logger logger = LoggerFactory.getLogger(ElasticsearchDAO.class);

    private final ObjectMapper mapper;

    private final RestHighLevelClient client;



    public ElasticsearchDAO(ObjectMapper mapper) throws IOException {
        String clusterUrl = "http://localhost:9200";
        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("elastic","c"));

        this.client = new RestHighLevelClient(RestClient.builder(HttpHost.create(clusterUrl))
                .setHttpClientConfigCallback(hcb -> hcb.setDefaultCredentialsProvider(credentialsProvider)));

        MainResponse info = this.client.info(RequestOptions.DEFAULT);
        logger.info("Connecte to {}  running version {}",clusterUrl,info.getVersion().getNumber());

        this.mapper = mapper;
    }

    public void save(Book book) throws IOException {
        String string = mapper.writeValueAsString(book);
        client.index(new IndexRequest("books").id(book.getId()).source(string, XContentType.JSON),RequestOptions.DEFAULT);
    }

    public void save(Author author) throws IOException{
        String string = mapper.writeValueAsString(author);
        client.index(new IndexRequest("authors").id(author.getId()).source(string,XContentType.JSON),RequestOptions.DEFAULT);
    }


    public void save(User user) throws IOException{
        String string = mapper.writeValueAsString(user);
        client.index(new IndexRequest("authors").id(user.getId()).source(string,XContentType.JSON),RequestOptions.DEFAULT);
    }


   /* public SearchResponse search(QueryBuilder queryBuilder) throws IOException {
        logger.debug("elasticsearch query: {}", queryBuilder.toString());

        SearchResponse response = client.search(new SearchRequest("book")
                        .source(new SearchSourceBuilder()
                        .query(queryBuilder)
                        .trackTotalHits(true)
                        .sort(SortBuilders.scoreSort())
                        .sort(SortBuilders.fieldSort("title"))), RequestOptions.DEFAULT);
        logger.debug("elastic search response: {} hits", response.getHits().getTotalHits());
        logger.trace("elastic search response: {} hits", response.toString());

        return response;

    }*/

    /*private ActionListener<PostDataRequest> listener = new ActionListener<PostDataRequest>() {
        @Override
        public void onResponse(PostDataRequest postDataRequest) {
            listener.onResponse();
        }

        @Override
        public void onFailure(Exception e) {

        }
    }*/
}

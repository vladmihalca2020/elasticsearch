package com.example.javaindexinges.service;

import com.example.javaindexinges.elasticDAO.ElasticsearchDAO;
import com.example.javaindexinges.model.Author;
import com.example.javaindexinges.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorService {

    private AuthorRepository authorRepository;
    private ElasticsearchDAO elasticsearchDAO;

    @Autowired
    public AuthorService(AuthorRepository authorRepository, ElasticsearchDAO elasticsearchDAO) {
        this.authorRepository = authorRepository;
        this.elasticsearchDAO = elasticsearchDAO;
    }

    public Author save(Author author){
        authorRepository.save(author);
        try {
            elasticsearchDAO.save(author);
        }   catch (Exception e){
            e.printStackTrace();
        }
        return author;
    }

}

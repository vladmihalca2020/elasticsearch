package com.example.javaindexinges.controller;

import com.example.javaindexinges.model.Author;
import com.example.javaindexinges.service.AuthorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthorController {

    @Autowired
    private AuthorService authorService;

    @PutMapping("/author")
    public Author saveAuthor(@RequestBody Author author){
        return authorService.save(author);
    }

}
